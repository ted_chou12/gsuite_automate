<?php
ini_set("display_startup_errors", 1);
ini_set("display_errors", 1);

require_once 'vendor/autoload.php';

session_start();
$client = new Google_Client();
$client->setAuthConfig('client_secrets.json');
$client->addScope('https://apps-apis.google.com/a/feeds/domain/');

if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
  $client->setAccessToken($_SESSION['access_token']);
  //$drive_service = new Google_Service_Drive($client);
  //$files_list = $drive_service->files->listFiles(array())->getItems();
  //echo json_encode($files_list);
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_URL, 'https://apps-apis.google.com/a/feeds/domain/2.0/gated1.hdetrial.jp/sso/general');
  curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Authorization: OAuth '. $_SESSION['access_token']['access_token']));
  curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
  curl_setopt($ch, CURLOPT_POSTFIELDS, array());
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
  $result = curl_exec($ch);
  curl_close($ch);

  echo '<pre>';
  var_dump(json_decode($result));
} else {
  $redirect_uri = 'http://' . $_SERVER['HTTP_HOST'] . '/web-php-simple/gsuite_automate/oauth2callback.php';
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}

//$client->revokeToken();
